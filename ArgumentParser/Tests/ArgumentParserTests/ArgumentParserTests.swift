import XCTest
@testable import ArgumentParser

final class ArgumentParserTests: XCTestCase {
    static var allTests = [
        ("testNormal", testNormal),
        ("testMandatoryFlagIsMissing", testMandatoryFlagIsMissing),
        ("testExpectedFlagParameterIsMissing", testExpectedFlagParameterIsMissing),
    ]
    
    func testNormal() {
        do {
            let rules: [ArgumentParser.Rule] = [
                ArgumentParser.Rule(flag: "-i", expectsParameter: true, isMandatory: true),
                ArgumentParser.Rule(flag: "-c", expectsParameter: true),
                ArgumentParser.Rule(flag: "-d"),
                ArgumentParser.Rule(flag: "-z"),
                ArgumentParser.Rule(flag: "-k", longFlag: "--kash"),
                ArgumentParser.Rule(flag: "-p", isMandatory: true)
            ]
            let options = try ArgumentParser(commandLineArguments: [String](CommandLine.arguments[1...]),
                                             rules: rules)
            print("foundArguments: \(options.matches)")
            print("extraArguments: \(options.extras)")
        } catch ArgumentParser.ArgumentError.mandatoryFlagIsMissing(let flag) {
            print("mandatoryFlagIsMissing: \(flag)")
            XCTFail()
        } catch ArgumentParser.ArgumentError.expectedFlagParameterMissing(let flag) {
            print("expectedParameterMissing: \(flag)")
            XCTFail()
        } catch {
            print("some other error")
            XCTFail()
        }
    }
    


    
    func testMandatoryFlagIsMissing() {
        do {
            let rules: [ArgumentParser.Rule] = [
                ArgumentParser.Rule(flag: "-i", expectsParameter: true, isMandatory: true),
                ArgumentParser.Rule(flag: "-p", isMandatory: true),
                ArgumentParser.Rule(flag: "-M", isMandatory: true)
            ]
            let options = try ArgumentParser(commandLineArguments: [String](CommandLine.arguments[1...]),
                                             rules: rules)
            print("foundArguments: \(options.matches)")
            print("extraArguments: \(options.extras)")
            XCTFail()
        } catch ArgumentParser.ArgumentError.mandatoryFlagIsMissing(let flag) {
            print("mandatoryFlagIsMissing: \(flag)")
            XCTAssert(flag == "-M", "exepected -M to be missing")
            // We expect this test to throw this error
        } catch ArgumentParser.ArgumentError.expectedFlagParameterMissing(let flag) {
            print("expectedParameterMissing: \(flag)")
            XCTFail()
        } catch {
            print("some other error")
            XCTFail()
        }
    }
    
    func testMandatoryLongFlagIsMissing() {
        do {
            let rules: [ArgumentParser.Rule] = [
                ArgumentParser.Rule(flag: "-i", expectsParameter: true, isMandatory: true),
                ArgumentParser.Rule(flag: "-p", isMandatory: true),
                ArgumentParser.Rule(flag: "--meat", isMandatory: true)
            ]
            let options = try ArgumentParser(commandLineArguments: [String](CommandLine.arguments[1...]),
                                             rules: rules)
            print("foundArguments: \(options.matches)")
            print("extraArguments: \(options.extras)")
            XCTFail()
        } catch ArgumentParser.ArgumentError.mandatoryFlagIsMissing(let flag) {
            print("mandatoryFlagIsMissing: \(flag)")
            XCTAssert(flag == "--meat", "exepected \(flag) to be missing")
            // We expect this test to throw this error
        } catch ArgumentParser.ArgumentError.expectedFlagParameterMissing(let flag) {
            print("expectedParameterMissing: \(flag)")
            XCTFail()
        } catch {
            print("some other error")
            XCTFail()
        }
    }


    func testExpectedFlagParameterIsMissing() {
        do {
            let rules: [ArgumentParser.Rule] = [
                ArgumentParser.Rule(flag: "-i", expectsParameter: true, isMandatory: true),
                ArgumentParser.Rule(flag: "-p", isMandatory: true),
                ArgumentParser.Rule(flag: "-m", expectsParameter: true)
            ]
            let options = try ArgumentParser(commandLineArguments: [String](CommandLine.arguments[1...]),
                                             rules: rules)
            print("foundArguments: \(options.matches)")
            print("extraArguments: \(options.extras)")
            XCTFail()
        } catch ArgumentParser.ArgumentError.mandatoryFlagIsMissing(let flag) {
            print("mandatoryFlagIsMissing: \(flag)")
            XCTFail()
        } catch ArgumentParser.ArgumentError.expectedFlagParameterMissing(let flag) {
            print("expectedParameterMissing: \(flag)")
            XCTAssert(flag == "-m", "exepected -m to be missing")
            // We expect this test to throw this error
        } catch {
            print("some other error")
            XCTFail()
        }
    }


    
    
}
