//
//  ArgumentParser.swift
//  ArgumentParser
//
//  Created by Zakk Hoyt on 4/25/20.
//  Copyright © 2020 Vaporwarewolf. All rights reserved.
//

import Foundation

//    getopt(args, options[, long_options]) -> opts, args
//
//    Parses command line options and parameter list.  args is the
//    argument list to be parsed, without the leading reference to the
//    running program.  Typically, this means "sys.argv[1:]".  shortopts
//    is the string of option letters that the script wants to
//    recognize, with options that require an argument followed by a
//    colon (i.e., the same format that Unix getopt() uses).  If
//    specified, longopts is a list of strings with the names of the
//    long options which should be supported.  The leading '--'
//    characters should not be included in the option name.  Options
//    which require an argument should be followed by an equal sign
//    ('=').
//
//    The return value consists of two elements: the first is a list of
//    (option, value) pairs; the second is the list of program arguments
//    left after the option list was stripped (this is a trailing slice
//    of the first argument).  Each option-and-value pair returned has
//    the option as its first element, prefixed with a hyphen (e.g.,
//    '-x'), and the option argument as its second element, or an empty
//    string if the option has no argument.  The options occur in the
//    list in the same order in which they were found, thus allowing
//    multiple occurrences.  Long and short options may be mixed.

public protocol AliasProvider {
    var aliases: [String] { get }
}

extension AliasProvider {
    public func aliasesEqual(_ aliasProvider: AliasProvider) -> Bool {
        aliases.filter { aliasProvider.aliases.contains($0) }.count > 0
    }
}

extension Argument {
    public var aliasesDescription: String {
        aliases.compactMap { $0 }.joined(separator: ", ")
    }
}

public protocol Argument: CustomStringConvertible {
    var flag: String { get }
    var longFlag: String? { get }
}

//extension Argument {
//    public func aliasesEqual(argument: Argument) -> Bool {
//        aliases.filter { argument.aliases.contains($0) }.count > 0
//    }
//}

extension Argument {
    var aliases: [String] { [flag, longFlag].compactMap { $0 } }
    func matches(flag: String) -> Bool { aliases.filter { $0 == flag }.count > 0 }
}


public struct ArgumentParser {
    public enum ArgumentError: LocalizedError {
        case mandatoryFlagIsMissing(flag: String)
        case expectedFlagParameterMissing(flag: String)
        
        public var errorDescription: String? {
            switch self {
            case .mandatoryFlagIsMissing(let flag): return "Mandatory argument flag (\(flag)) is not present."
            case .expectedFlagParameterMissing(let flag): return "Argument flag (\(flag)) was found, but no value was specified. (\(flag) <someValue>"
            }
        }
    }
    
    public struct Rule: Argument, AliasProvider, CustomStringConvertible {
        public let flag: String
        public let longFlag: String?
        public let expectsParameter: Bool
        public let isMandatory: Bool
        
        public var aliases: [String] { [flag, longFlag].compactMap { $0 } }
        func matches(flag: String) -> Bool { aliases.filter { $0 == flag }.count > 0 }
        
        public var description: String {
            let a = aliases.compactMap { $0 }
            let flagString: String = a.compactMap { #""\#($0)""# }.joined(separator: ", ")
            return "(\(flagString))"
        }

        public init(flag: String,
                    longFlag: String? = nil,
                    expectsParameter: Bool = false,
                    isMandatory: Bool = false) {
            self.flag = flag
            self.longFlag = longFlag
            self.expectsParameter = expectsParameter
            self.isMandatory = isMandatory
        }
    }
    
    public struct Match: AliasProvider /*: Argument*/ {
        public var aliases: [String] { rule.aliases }
        public let rule: Rule
        public let matchedFlag: String
        public let parameter: String?
    }
    
    public let commandLineArguments: [String]
    public let rules: [Rule]
    public let matches: [Match]
    public let extras: [String]
    public subscript(i: Int) -> Rule { rules[i] }

    /// Processes the list of commandLineArguments against the list of rules
    /// - Parameters:
    ///   - commandLineArguments: Pass the list in directly, or drop the first (binary name)
    ///          [String](CommandLine.arguments[1...])
    ///   - rules: A list of rules (ArgumentParser.Rule) to parse the arguments by.
    /// - Throws: This initializer will only throw errors of type ArgumentParser.ArgumentError
    public init(commandLineArguments: [String],
                rules: [Rule]) throws {
        self.commandLineArguments = commandLineArguments
        self.rules = rules
        
        var matches: [Match] = []
        var extras: [String] = []
        
        var shouldSkipNext = false
        for (i, flag) in commandLineArguments.enumerated() {
            if shouldSkipNext {
                shouldSkipNext = false
                continue
            }
            
            guard flag.prefix(1) == "-" else {
                extras.append(flag)
                continue
            }
            
            // Check if this arg is expected
            //guard let rule = rules.first(where: { $0.flag == flag }) else {
            guard let rule = rules.first(where: { $0.matches(flag: flag) }) else {
                extras.append(flag)
                continue
            }
            
            // Package flag and parameter into array.
            let match: Match = try {
                if rule.expectsParameter {
                    guard (i + 1) < commandLineArguments.count else {
                        throw ArgumentError.expectedFlagParameterMissing(flag: flag)
                    }
                    let nextFlag = commandLineArguments[i + 1]
                    guard nextFlag.prefix(1) != "-" else {
                        // We found another flag when we are expecting a parameter
                        throw ArgumentError.expectedFlagParameterMissing(flag: flag)
                    }
                    
                    shouldSkipNext = true
                    return Match(rule: rule, matchedFlag: flag, parameter: commandLineArguments[i + 1])
                } else {
                    return Match(rule: rule, matchedFlag: flag, parameter: nil)
                }
                }()
            
            matches.append(match)
        }
        
        // If we have an unmatched mandatory rule, throw an error
        let mandatoryRules = rules.filter { $0.isMandatory }
        if let unmatchedMandatoryRule = (mandatoryRules.first { mandatoryRule in
            //matches.filter { $0.aliasesEqual(argument: mandatoryRule) }.count == 0
            matches.filter { mandatoryRule.aliasesEqual($0) }.count == 0
        }) {
            throw ArgumentError.mandatoryFlagIsMissing(flag: unmatchedMandatoryRule.flag)
        }
        
        self.matches = matches
        self.extras = extras
    }
    
    /// Processes the list of commandLineArguments against the list of rules
    /// - Parameters:
    ///   - commandLineArguments: Pass the list in directly, or drop the first (binary name)
    ///          CommandLine.arguments[1...]
    ///   - rules: A list of rules (ArgumentParser.Rule) to parse the arguments by.
    /// - Throws: This initializer will only throw errors of type ArgumentParser.ArgumentError
    public init(commandLineArguments slice: ArraySlice<String>, rules: [Rule]) throws {
        try self.init(commandLineArguments: [String](slice), rules: rules)
    }
}

