# ArgumentParser

A description of this package.


Suppose you are running your swift binary from command line and wish to process the command line arguments:

````
import ArgumentParser

do {
    let rules: [ArgumentParser.Rule] = [
        ArgumentParser.Rule(flag: "-i", expectsParameter: true, isMandatory: true),
        ArgumentParser.Rule(flag: "-c", expectsParameter: true),
        ArgumentParser.Rule(flag: "-d"),
        ArgumentParser.Rule(flag: "-z"),
        ArgumentParser.Rule(flag: "-p", isMandatory: true)
    ]
    let options = try ArgumentParser(commandLineArguments: [String](CommandLine.arguments[1...]),
                                     rules: rules)
    print("foundArguments: \(options.matches)")
    print("extraArguments: \(options.extras)")
} catch ArgumentParser.ArgumentError.mandatoryFlagIsMissing(let flag) {
    print("mandatoryFlagIsMissing: \(flag)")
} catch ArgumentParser.ArgumentError.expectedFlagParameterMissing(let flag) {
    print("expectedParameterMissing: \(flag)")
} catch {
    print("some other error")
}
````
